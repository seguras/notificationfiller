// Decompiled with JetBrains decompiler
// Type: NotificationFiller.Command
// Assembly: NotificationFiller, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 24CBC20C-1635-4344-A8B6-A52931269DD3
// Assembly location: P:\Tools\Notifications\NotificationFiller\NotificationFiller.exe

using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Http;

namespace NotificationFiller
{
  public class Command
  {
    private HttpClient httpClient;

    public string WSUrl
    {
      get
      {
        return ConfigurationManager.AppSettings["WSUrl"];
      }
    }

    private HttpClient HttpClient
    {
      get
      {
        if (this.httpClient == null)
        {
          this.httpClient = new HttpClient();
          this.httpClient.BaseAddress = new Uri(this.WSUrl);
        }
        return this.httpClient;
      }
    }

    public string NotificationFolder { get; set; }

    public string LanguageCode { get; set; }

    public string TemplateName { get; set; }

    public string SubscriptionId { get; set; }

    public TextWriter Logger { get; set; }

    public Command(TextWriter logger)
    {
      this.Logger = logger;
    }

    public void Execute()
    {
      if (!new DirectoryInfo(this.NotificationFolder).Exists)
        throw new Exception(string.Format("Le dossier '{0}' n'existe pas", (object) this.NotificationFolder));
      if (string.IsNullOrEmpty(this.LanguageCode))
        this.ExecuteAllLanguage();
      else
        this.ExecuteForLanguage(this.LanguageCode);
    }

    private void ExecuteAllLanguage()
    {
      foreach (FileSystemInfo directory in new DirectoryInfo(this.NotificationFolder).GetDirectories())
        this.ExecuteForLanguage(directory.Name);
    }

    public void ExecuteForLanguage(string languageCode)
    {
      if (!new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode)).Exists)
        throw new Exception(string.Format("Le dossier pour la langue '{0}' n'existe pas", (object) languageCode));
      this.Logger.WriteLine(string.Format("Process language {0}", (object) languageCode));
      if (string.IsNullOrEmpty(this.SubscriptionId))
        this.ExecuteAllSubscription(languageCode);
      else
        this.ExecuteForSubscription(languageCode, this.SubscriptionId);
    }

    private void ExecuteAllSubscription(string languageCode)
    {
      foreach (DirectoryInfo directory in new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode)).GetDirectories())
        this.ExecuteForSubscription(languageCode, directory.Name);
    }

    public void ExecuteForSubscription(string languageCode, string SubscriptionId)
    {
      if (!new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode, SubscriptionId)).Exists)
        throw new Exception(string.Format("Le dossier pour la souscription '{0}' dans la langue '{1}' n'existe pas", (object) SubscriptionId, (object) languageCode));
      this.Logger.WriteLine(string.Format("  Process subscription {0}", (object) SubscriptionId));
      if (string.IsNullOrEmpty(this.TemplateName))
        this.ExecuteAllTemplates(languageCode, SubscriptionId);
      else
        this.ExecuteForTemplate(languageCode, SubscriptionId, string.Format("{0}.eml", (object) this.TemplateName));
    }

    public void ExecuteForTemplate(string languageCode, string SubscriptionId, string templateName)
    {
      FileInfo fileInfo = new FileInfo(Path.Combine(this.NotificationFolder, languageCode, SubscriptionId, templateName));
      if (!fileInfo.Exists)
        throw new Exception(string.Format("Le template '{0}' pour la souscription '{1}' dans la langue '{2}' n'existe pas", (object) templateName, (object) SubscriptionId, (object) languageCode));
      this.Logger.WriteLine(string.Format("    Process template {0}", (object) templateName));
      string requestUri = string.Format("{0}/{1}", (object) Path.GetFileNameWithoutExtension(templateName), (object) languageCode);
      if (SubscriptionId != "global")
        requestUri += string.Format("/{0}", (object) SubscriptionId);
      using (FileStream fileStream = File.OpenRead(fileInfo.FullName))
      {
        HttpClient httpClient = new HttpClient();
        using (MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString((IFormatProvider) CultureInfo.InvariantCulture)))
        {
          multipartFormDataContent.Add((HttpContent) new StreamContent((Stream) fileStream), "content", templateName);
          HttpResponseMessage result = this.HttpClient.PutAsync(requestUri, (HttpContent) multipartFormDataContent).Result;
          if (result.IsSuccessStatusCode)
            return;
          this.Logger.WriteLine(string.Format("Error: {0} {1}", (object) result.StatusCode, (object) result.ReasonPhrase));
        }
      }
    }

    public void ExecuteAllTemplates(string languageCode, string SubscriptionId)
    {
      foreach (FileInfo file in new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode, SubscriptionId)).GetFiles("*.eml"))
        this.ExecuteForTemplate(languageCode, SubscriptionId, file.Name);
    }
  }
}
